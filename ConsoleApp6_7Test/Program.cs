﻿using System;
using System.Threading.Tasks;
using TodoList.Task;

namespace TodoList
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var work = new FileWork();

            ToDoList myTasks = new();

            myTasks.Load();

            var a = work.FindTaskBy(Сriterion.priority, 1, "Medium");

            Print(myTasks);
        }

        public static void Print(ToDoList myTasks)
        {
            for (int i = 0; i < myTasks.Count; i++)
            {
                Console.WriteLine($"├── {i + 1} Список");
                for (int j = 0; j < myTasks[i].Count; j++)
                {
                    if (j == myTasks[i].Count - 1) Console.Write("│   └── ");
                    else Console.Write("│   ├── ");
                    Console.WriteLine($"{j + 1}: " +
                                      $"{myTasks[i, j].Name,8}   " +
                                      $"{myTasks[i, j].Start_Date}   " +
                                      $"{myTasks[i, j].End_Date}   " +
                                      $"{myTasks[i, j].Priority}   " +
                                      $"{myTasks[i, j].Tag}  ");
                }
            }
        }
    }
}