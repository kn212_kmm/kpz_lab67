﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;


namespace TodoList.Task
{
    /// <summary>
    /// Шаблон делегирования<code></code>
    /// Делегирование (англ. Delegation) — основной шаблон проектирования, 
    /// в котором объект внешне выражает некоторое поведение, 
    /// но в реальности передаёт ответственность за выполнение этого поведения связанному объекту. 
    /// Шаблон делегирования является фундаментальной абстракцией,
    /// на основе которой реализованы другие шаблоны - композиция (также называемая агрегацией), 
    /// примеси (mixins) и аспекты (aspects).
    /// <code></code>
    /// Плюсы:<code>
    /// - Возможность изменить поведение конкретного экземпляра объекта вместо создания 
    /// нового класса путём наследования.</code>
    /// <code></code>
    /// Минусы:<code>- Этот шаблон обычно затрудняет оптимизацию 
    /// по скорости в пользу улучшенной чистоты абстракции.</code>
    /// </summary>
    public class MyTask
    {
        public string Name { get; private set; }
        public DateTime? Start_Date { get; private set; }
        public DateTime? End_Date { get; private set; }
        public PriorityEnum Priority { get; private set; }
        public string Tag { get; set; }

        public MyTask(string Name, DateTime? Start_Date, DateTime? End_Date, PriorityEnum Priority, string Tag)
            : this(Name, End_Date, Priority, Tag)
        {
            this.Start_Date = Start_Date; 
        }

        public MyTask(string Name, DateTime? End_Date,  PriorityEnum Priority, string Tag)
        {
            this.Name = Name;
            Start_Date = DateTime.UtcNow;
            this.End_Date = End_Date;
            this.Priority = Priority;
            this.Tag = Tag;
        }

        internal void Add(MyTask myTask)
        {
            throw new NotImplementedException();
        }
    }

    public class TaskList : IList<MyTask>, 
                     IEnumerable<MyTask>,
                     IReadOnlyList<MyTask>,
                     ICloneable
    {
        private readonly List<MyTask> tasks = new();

        public int Count => tasks.Count;

        public bool IsReadOnly => true;

        public MyTask this[int index]
        {
            get => tasks[index];
            set => tasks[index] = value;
        }
        public MyTask? this[string name]
        {
            get => (MyTask?)(from task in tasks where name == task.Name select task);
        }

        public void Add(MyTask task)
        {
            tasks.Add(task);
        }
  
        public int GetIndexOfTaskByName(string name, int offset)
        {
            return tasks.FindIndex(offset, x => x.Name == name);
        }

        public int GetIndexOfTaskByName(string name)
        {
            return GetIndexOfTaskByName(name, 0);
        }

        public int IndexOf(MyTask item)
        {
            return tasks.IndexOf(item);
        }

        public void Insert(int index, MyTask item)
        {
            tasks.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            tasks.RemoveAt(index);
        }

        public void Clear()
        {
            tasks.Clear();
        }

        public bool Contains(MyTask item)
        {
            return tasks.Contains(item);
        }

        public void CopyTo(MyTask[] array, int arrayIndex)
        {
            tasks.CopyTo(array, arrayIndex);
        }

        bool ICollection<MyTask>.Remove(MyTask item)
        {
            return tasks.Remove(item);
        }

        public IEnumerator<MyTask> GetEnumerator()
        {
            return tasks.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}