﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TodoList.Task;

namespace TodoList
{
    public class ToDoList : IList<TaskList>
    {
        private FileWork fileWork = new FileWork();
        private List<TaskList> list = new();

        public TaskList this[int index]
        {
            get { return list[index]; }
            set { list[index] = value; }
        }

        public ToDoList()
        {

        }

        public MyTask this[int _list, int _task]
        {
            get => list[_list][_task];
        }
        TaskList IList<TaskList>.this[int index] 
        {
            get => list[index];
            set
            {
                if (index >= Count)
                    list.Add(value);
                else
                    list[index] = value;
            }
        }

        public int Count => list.Count;
        public int Last_Id = FileWork.GetGreatestListId();

        public bool IsReadOnly => false;

        public void Add(TaskList tasks)
        {
            list.Add(tasks);
        }

        public void NewList()
        {
            list.Add(new());
        }

        public void Add(MyTask item)
        {
            list.Add(new() { item });
        }


        public void Clear()
        {
            list.Clear();
        }

        public bool Contains(TaskList item)
        {
            return Contains(item);
        }

        public void CopyTo(TaskList[] array, int arrayIndex)
        {
            CopyTo(array, arrayIndex);
        }

        public void CopyTo(MyTask[] array, int arrayIndex)
        {
            CopyTo(array, arrayIndex);
        }

        public IEnumerator<TaskList> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        public int IndexOf(TaskList item)
        {
            return IndexOf(item);
        }


        public void Insert(int index, TaskList item)
        {
            
        }
      
        public bool Remove(TaskList item)
        {
            return list.Remove(item);
        }

        public void RemoveAt(int index)
        {
            list.RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Save()
        {
            for (int i = 1; i <= list.Count; i++)
            {
                
                if (!FileWork.IsAvailableList(i)) 
                    fileWork.AddList(i);

                foreach (MyTask item in list[i - 1])
                    fileWork.AddTask(i, item);
            }
        }

        public void Load()
        {
            for (int i = 1; i <= Last_Id; i++)
            {
                if(FileWork.IsAvailableList(i))
                    list.Add(fileWork.GetTasksByListId(i));
            }
        }

        //public MyTask Find(Сriterion critery, string value)
        //{

        
        //}
    }
}
