﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml.Linq;
using TodoList.Task;

namespace TodoList
{
    public class FileWork
    {
        public static int GetGreatestTaskId(int listId)
        {
            return Convert.ToInt32(
                   XDocument
                  .Load(Path.GetPath)?
                  .Element("main")?
                  .Elements("list")?
                  .FirstOrDefault(l => l?.Attribute("list_id")?.Value == $"{listId}")?
                  .Elements("task")?
                  .LastOrDefault()?
                  .Attribute("task_id")?.Value);
        }
        public static int GetGreatestListId()
        {
            return Convert.ToInt32(
                   XDocument
                  .Load(Path.GetPath)?
                  .Element("main")?
                  .Elements("list")?
                  .LastOrDefault()?
                  .Attribute("list_id")?.Value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsAvailableList(int listId)
        {
            return XDocument
               .Load(Path.GetPath)?
               .Element("main")?
               .Elements("list")?
               .FirstOrDefault(l => l.Attribute("list_id")?
               .Value == $"{listId}") == null ? false : true;
        }

        public TaskList FindTaskBy(Сriterion сriterion, int listId, string value)
        {
            var root = XDocument.Load(Path.GetPath).Element("main");

            var TaskList = new TaskList();

            var res = root.Element("list")?.Elements("task").Where(t => t?.Attribute(сriterion.ToString().ToLower())?.Value == value);

            if (!res.Any())
                throw new Exception("Ti pidor");

            foreach (var item in res)
            {
                var xName = item.Value;
                var xStart = item.Attribute("start_date");
                var xEnd = item.Attribute("end_date");
                var xPriotity = item.Attribute("priority");
                var xTag = item.Attribute("tag");

                var task = new MyTask(xName, DateTime.Parse(xStart!.Value), DateTime.Parse(xEnd!.Value), GetPriority(xPriotity?.Value), xTag?.Value);
                TaskList.Add(task);
            }

            return TaskList;

        }

        public bool AddTask(int listId, MyTask newTask)
        {
            var root = XDocument.Load(Path.GetPath).Element("main");
            var list = root?.Elements("list")
                           .FirstOrDefault(l => l?.Attribute("list_id")?.Value == $"{listId}");
            
            if (list == null)
                return false;
            

            list.Add(new XElement("task",
                new XAttribute("task_id", GetGreatestTaskId(listId) + 1),
                new XAttribute("start_date", newTask.Start_Date == null ? "" : newTask.Start_Date.Value.ToString("r", CultureInfo.GetCultureInfo("de-DE"))),
                new XAttribute("end_date", newTask.End_Date == null ? "" : newTask.End_Date.Value.ToString("r", CultureInfo.GetCultureInfo("de-DE"))),
                new XAttribute("priority", newTask.Priority),
                new XAttribute("tag", newTask.Tag),
                newTask.Name
                )
            );
            root?.Save(Path.GetPath);
            return true;

        }
        
        public bool AddList(int? listId)
        {
            var root = XDocument.Load(Path.GetPath).Element("main");

            root?.Add(new XElement("list", new XAttribute("list_id", listId ?? GetGreatestListId() + 1)), "");

            root?.Save(Path.GetPath);
            return true;
        }

        public bool DeleteTask(int listId, int id)
        {
            var xDocument = XDocument.Load(Path.GetPath).Element("main");
            var obj = xDocument?
                     .Elements("list")?
                     .FirstOrDefault(l => l?.Attribute("list_id")?.Value == $"{listId}")?
                     .Elements("task")
                     .FirstOrDefault(t => t?.Attribute("task_id")?.Value == $"{id}");

            if (xDocument == null)
                return false;
            
            obj?.Remove();
            obj?.Save(Path.GetPath);
            return true;
        }

        public bool UpdateTask(int listId, int taskId, MyTask updatedTask)
        {
            var xDocument = XDocument.Load(Path.GetPath).Element("main");
            var list = xDocument?.Elements("list").FirstOrDefault(l => l.Attribute("list_id")?.Value == $"{listId}");

            if (list == null)
                return false;

            var task = list.Elements("task").FirstOrDefault(t => t.Attribute("task_id")?.Value == $"{taskId}");
            if (task == null)
                return false;

            var xName = task.Attribute("name");
            var xStart = task.Attribute("start_date");
            var xEnd = task.Attribute("end_date");
            var xPriotity = task.Attribute("priority");
            var xTag = task.Attribute("tag");

            task.Value = updatedTask.Name;
            xStart.Value = updatedTask.Start_Date == null ? "" : updatedTask.Start_Date.Value.ToString("r", CultureInfo.GetCultureInfo("de-DE"));
            xEnd.Value = updatedTask.End_Date == null ? "" : updatedTask.End_Date.Value.ToString("r", CultureInfo.GetCultureInfo("de-DE"));
            xPriotity.Value = updatedTask.Priority.ToString();
            xTag.Value = updatedTask.Tag;

            xDocument?.Save(Path.GetPath);
            return true;
        }

        public XElement? GetTaskFromListById(int listId, int id)
        {
            return XDocument.Load(Path.GetPath)
                  .Element("main")?
                  .Elements("list")
                  .FirstOrDefault(l => l.Attribute("list_id")?.Value == $"{listId}")?
                  .Elements("task")
                  .FirstOrDefault(t => t.Attribute("task_id")?.Value == $"{id}");
        }

        public TaskList GetTasksByListId(int id)
        {
            TaskList list = new ();
            var tasks = XDocument.Load(Path.GetPath)
                                 .Element("main")?
                                 .Elements("list")?
                                 .FirstOrDefault(l => l.Attribute("list_id")?.Value == $"{id}")?
                                 .Elements("task");

            foreach (var item in Path.IsRefNull(tasks))
            {
                //DateTime? startDate = item?.Attribute("end_date")?.Value;
                MyTask task = new(item.Value,
                                  DateTime.Parse(item!.Attribute("end_date")!.Value),
                                  GetPriority(item.Attribute("priority")?.Value), 
                                  item.Attribute("tag")!.Value);
                list.Add(task);
            }
            return list;
        }

        public static PriorityEnum GetPriority(string? priority)
        {
            return priority switch
            {
                "High" => PriorityEnum.High,
                "Medium" => PriorityEnum.Medium,
                _ => PriorityEnum.Low,
            };
        }
    }

    public static class Path
    {
        public static string GetPath => Environment
                                       .CurrentDirectory
                                       .Replace("\\bin\\Debug\\net6.0", "\\Todolist.xml");

        public static T IsRefNull<T>(T? value)
        {
            return value != null ? value : throw new NullReferenceException();
        }
    }

}
