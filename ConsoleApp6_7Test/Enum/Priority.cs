﻿namespace TodoList
{
    public enum PriorityEnum
    {
        Low = 0,
        Medium,
        High
    }

    public enum Сriterion
    {
        Name = 0,
        start_date,
        end_date,
        priority,
        tag
    }
}
